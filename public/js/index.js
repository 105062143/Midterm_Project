function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            //var user_password = user.password;
            menu.innerHTML = "<span id='user-btn' class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var user_page = document.getElementById("user-btn");
            user_page.addEventListener('click', function(){
                document.getElementById('container').innerHTML="<div align='center'><img src='img/user.png' alt='' class='mr-2 rounded' style='height:256px; width:256px;'></div><div align='center'><div><h4>Accont/Email: </h4><h6>"+user_email+"</h6></div><h4>Password: </h4><h6>censored</h6></div></div>";
            });

            var logout = document.getElementById("logout-btn");
            logout.addEventListener('click', function(){
                firebase.auth().signOut();
            });
            show_post_list();
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('container').innerHTML = "<div align='center' style=' display:flex;width:100%;height:200px;align-items:center;justify-content:center;'><h1 class='animated infinite bounce' style='color:purple;'> Please Log In </h1></div>";
        }
    });

}

window.onload = function () {
    init();
};

function show_post_list(){
    var new_post_btn_item = "<div class='media text-muted pt-3'><button id='new_post_btn' type='button' class='btn btn-success'>New Post</button></div>";
    var post_item = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Title</h6><textarea class='form-control' rows='1' id='post_title' style='resize:none'></textarea><h6 class='border-bottom border-gray pb-2 mb-0'>Content</h6><textarea class='form-control' rows='10' id='post_content' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='post_btn' type='button' class='btn btn-success'>Post</button></div></div>";
    
    var postsRef = firebase.database().ref('post_list');
    // List for store posts html
    var total_post = [];
    var post_list = [];
    
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData=childSnapshot.val();
                total_post[total_post.length]="<div class='my-3 p-3 bg-white rounded box-shadow'><a href='javascript:comment_handler(&quot;"+childData.value+"&quot;);'>"+childData.title+"</a><small>        by "+childData.email+"</small></div>\n";
            });
            post_list = total_post.reverse();
            document.getElementById('container').innerHTML=new_post_btn_item+post_list.join('');
            new_post_btn = document.getElementById('new_post_btn');
            new_post_btn.addEventListener('click', function () {
                document.getElementById('container').innerHTML=post_item;
                post_btn = document.getElementById('post_btn');
                post_title = document.getElementById('post_title');
                post_txt = document.getElementById('post_content');
                post_btn.addEventListener('click', function () {
                    if (post_title.value != "" && post_txt.value != "") {
                        var d = new Date();
                        var num = Math.floor(Math.random()).toString();
                        var time = d.getFullYear().toString() + d.getMonth().toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString() + d.getSeconds().toString();
                        firebase.database().ref('post_list/').push({
                            title:post_title.value,
                            value:time+num,
                            email:firebase.auth().currentUser.email
                        });
                        firebase.database().ref('comment_list/'+time+num+'/').push({
                            message:post_txt.value,
                            email:firebase.auth().currentUser.email
                        });
                        post_title.value="";
                        post_txt.value="";
                        window.location = "index.html";
                    }
                    else {
                        if (post_title.value == "" && post_txt.value != "") {
                            alert("Where is the fucking title?");
                        }
                        else if (post_title.value != "" && post_txt.value == "") {
                            alert("Where is the fucking conten?");
                        }
                        else {
                            alert("Are you fucking kidding me?");
                        }
                    }
                });
            });
        
        })
        .catch(e => console.log(e.message));
}

function comment_handler(index) {
    document.getElementById('container').innerHTML="";
    
    var comment_item = "<div class='my-3 p-3 bg-white rounded box-shadow'><h5 class='border-bottom border-gray pb-2 mb-0'>New Comment</h5><textarea class='form-control' rows='5' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='comment_btn' type='button' class='btn btn-success'>Submit</button></div></div>";
    // The html code for post
    var str_first_floor = "<div class='my-3 p-3 bg-white rounded box-shadow'><h1 class='border-bottom border-gray pb-2 mb-0'>Top</h1><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>"
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var commentsRef = firebase.database().ref('comment_list/'+index.toString());
    // List for store posts html
    var total_comment = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    commentsRef.once('value')
        .then(function (snapshot) {
			snapshot.forEach(function(childSnapshot){
                var childData=childSnapshot.val();
                if (total_comment.length == 0) total_comment[total_comment.length]=str_first_floor+childData.email+"</strong>"+childData.message+str_after_content;
				else total_comment[total_comment.length]=str_before_username+childData.email+"</strong>"+childData.message+str_after_content;
				first_count+=1;
			});
            document.getElementById('container').innerHTML=total_comment.join('')+comment_item;
            comment_btn = document.getElementById('comment_btn');
            comment_txt = document.getElementById('comment');
            comment_btn.addEventListener('click', function () {
                if (comment_txt.value != "") {
                    firebase.database().ref('comment_list/'+index.toString()+'/').push({
                        message:comment_txt.value,
                        email:firebase.auth().currentUser.email
                    });
                    comment_txt.value="";
                }
            });

			commentsRef.on('child_added',function(data){
				second_count+=1;
				if(second_count>first_count){
					var childData=data.val();
					total_comment[total_comment.length]=str_before_username+childData.email+"</strong>"+childData.message+str_after_content;
					document.getElementById('container').innerHTML=total_comment.join('')+comment_item;
                }
                
                comment_btn = document.getElementById('comment_btn');
                comment_txt = document.getElementById('comment');
                comment_btn.addEventListener('click', function () {
                    if (comment_txt.value != "") {
                        firebase.database().ref('comment_list/'+index.toString()+'/').push({
                            message:comment_txt.value,
                            email:firebase.auth().currentUser.email
                        });
                        comment_txt.value="";
                    }
                });
			});
			
        })
        .catch(e => console.log(e.message));
};