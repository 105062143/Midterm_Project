# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. postlist page
    4. leave comment under any post
* Other functions (add/delete)
    1. [xxx]
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
    作品網址：https://software-studio-105062143.firebaseapp.com
    報告網址：https://gitlab.com/105062143/Midterm_Project/blob/master/README.md
    Basic Component:
        * Membership Mechanism: 需要登入才顯示postlist page。
        * GitLab Page: 改為deploy在firebase上。
        * Database: postlist及各post的comment，即透過firebase的database。
        * RWD: 套用Bootstrap的SignIn及Offcanvas樣板，達成RWD的效用。
    Advanced Component:
        * Third-Party Sign In: 在Sign In頁面，可選擇使用Google帳戶登入。
        * Use CSS Animation: 在未登入前，在首頁(index.html)會顯示CSS動畫提示登入。
    Topic Key Function: 
        * user page: 在登入後點擊左上角選單按鈕，選擇自己的登入信箱，即可跳轉到userpage。
        * post page: 點擊postlist上任一post便可看到此post及其comment，頁面最下方可留comment。
        * postlist page: 登入後即可看見，在任一頁面點擊左上Simple Forum按鈕也可回到postlist page。點擊上方的New Post按鍵可發表新的post。
        * leave comment under any post: 在任一post頁面最下方可留comment。
## Security Report (Optional)
